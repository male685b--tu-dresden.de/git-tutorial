# Well done!

You have succesfully cloned this project to your local machine. 
You can now do some changes, create new commits, etc. 
However: You cannot push your changes to the remote repository as you don't have the necessary permissions. 
In part 2, you will learn how to create your own remote repositories and how to interact with it, including proper authenication.
